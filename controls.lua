table.insert(ctrls,{
  Name = "named.mediacast.router.component", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})
for output=1, 32 do
  table.insert(ctrls, {
    Name = string.format("select.%s", output),
    ControlType = "Text",
    Count = 1,
    PinStyle = "Both",
    UserPin = true
  })
  for input=1, 32 do
    table.insert(ctrls, {
      Name = string.format("output.%s.input.%s.select", output, input),
      ControlType = "Button",
      ButtonType = "Toggle",
      Count = 1,
      PinStyle = "Both",
      UserPin = true
    })

    table.insert(ctrls, {
      Name = string.format("output.%s.input.%s.select.indicator", output, input),
      ControlType = "Indicator",
      IndicatorType = "LED",
      Count = 1,
      PinStyle = "Output",
      UserPin = true
    })
  end
end
