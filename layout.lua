table.insert(graphics,{
  Type = "Label", Text = "Mediacast Router Name",
  HTextAlign = "Left",
  VTextAlign = "Center",
  Position = {4,4}, Size = {126,16},
})

--[[layout["Code"] = {
  Style = "Text", TextBoxStyle = "Normal", 
  Position = {0,0}, Size = {0,0}
}]]


layout["named.mediacast.router.component"] = {
  PrettyName = "Mediacast Router Component",
  Style = "ComboBox",
  TextBoxStyle = "Normal",
  Position = {130,4},
  HTextAlign = "Center",
  Size = {120,16},
}

groupbox_x = 0
groupbox_y = 0

for i=1, props["Max Outputs"].Value do
  groupbox_x = i*40
  for j=1, props["Max Inputs"].Value do
    groupbox_y = j*24
  end  
end


  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Input",
    StrokeWidth = 1,
    Position = {4,70},
    Size = {groupbox_x + 60, groupbox_y+26},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Output",
    StrokeWidth = 1,
    Position = {60,30},
    Size = {groupbox_x + 4,groupbox_y + 66},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

local function GetIPos(ix, base_x, base_y)
    local base={ x = base_x, y = base_y }
    local ofs={ x = 0, y = 24 }
    local col,row = (ix-1),(ix-1)
    x = base.x + col*ofs.x
    y = base.y + row*ofs.y
    return x, y
end

for i=1, props["Max Outputs"].Value do
  table.insert(graphics,{
    Type = "Label", Text = string.format("%s", i),
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {(i*40)+20,48}, Size = {40,16},
  })
  layout[string.format("select.%s", i)] = {
    PrettyName = string.format("Output %s~Select", i),
    Style = "TextBox",
    TextBoxStyle = "Normal",
    Position = {24+(i*40), 74},
    HTextAlign = "Center",
    Size = {36,16}
  }
  for j=1, props["Max Inputs"].Value do
    pos_x, pos_y = GetIPos(j, 24+(i*40), 96)
    if i == 1 then
      table.insert(graphics,{
        Type = "Label", Text = string.format("%s", j),
        HTextAlign = "Right",
        VTextAlign = "Center",
        Position = {4,(j*24)+70}, Size = {52,24},
      })
    end
    size_x = 36
    size_y = 20

    border = 2

    base_name = string.format("output.%s.input.%s.select", i, j)


    layout[base_name] = {
      PrettyName = string.format("Output %s~Input %s Select", i, j),
      Style = "Button",
      Position = {pos_x+3, pos_y},
      Size = {size_x-6, size_y},
      CornerRadius = 0,
      ZOrder = 10000+i+(2*j),
    }

    layout[base_name..".indicator"] = {
      PrettyName = string.format("Output %s~Input %s Select Indicator", i, j),
      Style = "Button",
      ButtonVisualStyle = "Flat",
      Position = {pos_x +3 - border, pos_y - border},
      Size = {size_x + (border)-4, size_y + (border*2)},
      CornerRadius = 0,
      StrokeWidth = 0,
      Color = {50,150,255},
      ZOrder = 10000
    }
  end
end