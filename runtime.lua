function hasProperty(ctl, prop)
  if prop == "IsDisabled" then
    return pcall(function() local tmp = ctl.IsDisabled end)
  elseif prop == "Choices" then
    return pcall(function() local tmp = ctl.Choices end)
  elseif prop == "Legend" then
    return pcall(function() local tmp = ctl.Legend end)
  elseif prop == "IsIndeterminate" then
    return pcall(function() local tmp = ctl.IsIndeterminate end)
  end
end

function updateDisableds()
  for k,v in pairs (Controls) do
    for kk,vv in pairs (virtual_controls) do
      if vv[3] == k then
        v.IsDisabled = false
        if string.find(k, "output") then
          Controls[vv[3]..".indicator"].IsDisabled = false
          Controls[vv[3]..".indicator"].Value = vv[2].Value
        end
      end
    end
  end
  updateLegends()
end

function updateLegends() 
  for k,v in pairs(virtual_controls) do
    if hasProperty(v[2], "Legend") then
      Controls[v[3]].Legend = v[2].Legend
    end
  end
end

function resetControls()
  for k,v in pairs (Controls) do
    if k ~= "named.mediacast.router.component" then
      v.IsDisabled = true
      if hasProperty(v, "Legend") then
        v.Legend = ""
      end
      v.Value = 0
    end
  end
end

function updateIndicators()
  for k,v in pairs (virtual_controls) do
    if string.find(v[3], "output") then
      Controls[v[3]..".indicator"].Value = v[2].Value
    end
  end
end

UpdateLegendsTimer = Timer.New()
UpdateLegendsTimer.EventHandler = updateLegends

function handleControl(ctl)
  for k,v in pairs (virtual_controls) do
    if ctl == v[2] then
      Controls[v[3]].String = v[2].String
    end
    if ctl.Index == v[1] then
      v[2].Value = ctl.Value
      v[2].String = ctl.String
    end
  end
  updateIndicators()
end

function assignControls(component)
  virtual_controls = {}
  COMP = component
  for name, control in pairs (COMP) do
    if Controls[name] then
      --print(name, control)
      control.EventHandler = handleControl
      table.insert(virtual_controls, {Controls[name].Index, control, tostring(name)}) 
      Controls[name].String = control.String
      
    end
  end
  resetControls()
  updateIndicators()
  UpdateLegendsTimer:Start(1)
  updateDisableds()
end

function updateNamedComponent()
  if Controls["named.mediacast.router.component"].String ~= "" and validChoice() then
    Bridge = Component.New(Controls["named.mediacast.router.component"].String)
    assignControls(Bridge)
  else
    Controls["named.mediacast.router.component"].String = ""
  end
end


named_routers = {}
for k,v in pairs (Component.GetComponents()) do
  for kk, vv in pairs(v) do
    if vv == "video_router" then
      table.insert(named_routers, v["ID"])
    end
  end
end

function validChoice()
  name_exists = false
  for k,v in pairs (named_routers) do
    if v == Controls["named.mediacast.router.component"].String then
      name_exists = true
    end
  end
  return name_exists
end

for i=1, 32 do
  for j=1, 32 do
    Controls["output."..i..".input."..j..".select.indicator"].IsDisabled = false
  end
end

Controls["named.mediacast.router.component"].Choices = named_routers

updateNamedComponent()
for c, ctl in pairs (Controls) do
  ctl.EventHandler = handleControl
end
Controls["named.mediacast.router.component"].EventHandler = updateNamedComponent