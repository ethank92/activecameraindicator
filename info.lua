PluginInfo = {
  Name = "Active Camera Indicator",
  Version = "0.0",
  BuildVersion = "0.0.0.0",
  Id = "661c78bb-55b7-4787-b4cc-42cda1fa70ae",
  Author = "Ethan Klein",
  Description = "A plugin to indicate current selections of cameras for a given mediacast router"  
}