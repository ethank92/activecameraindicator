--[[ Properties File Contents ]]
table.insert(props,{
  Name = "Max Inputs",
  Type = "integer",
  Min = 1, Max = 32, Value = 2
})

table.insert(props,{
  Name = "Max Outputs",
  Type = "integer",
  Min = 1, Max = 32, Value = 1
})

table.insert(props, {
  Name = "Debug Print",
  Type = "enum",
  Choices = {"None", "Tx/Rx", "Tx", "Rx", "Function Calls", "All"},
  Value = "None"
})
